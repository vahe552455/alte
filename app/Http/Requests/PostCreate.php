<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'title'=>'required|string|min:5',
          'category_id'=>'required',
          'description'=>'required|string',
          'path[]'=>'mimes:jpeg,jpg,png,gif|max:10000'
        ];
    }
}
