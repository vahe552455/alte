<?php

namespace App\Http\Controllers\Category;

use App\Contracts\CategoryInterface;
use App\Http\Requests\CategoryCreate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category;

    public function __construct(CategoryInterface $category)
    {       //category repo
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->all();
        return view('category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreate $request)
    {
       $data =  $this->category->store($request);
       return redirect()->back()->with('message','created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $category = $this->category->edit($id);
       return view('category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illumиnate\Http\Response
     */
    public function update(CategoryCreate $request, $id)
    {
         $category = $this->category->update($id, $request) ;
         return redirect()->back()->with('message','Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data  =  $this->category->delete($id);
        if($data) {
            return response()->json(['id'=>$id]);
        }
  }
}
