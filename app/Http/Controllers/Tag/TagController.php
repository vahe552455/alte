<?php

namespace App\Http\Controllers\Tag;

use App\Contracts\PostInterface;
use App\Http\Requests\TagCreate;
use App\Repositories\TagRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\TagInterface;
class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $tagRepo;
    protected $postRepo;

    public function __construct(TagInterface $tag, PostInterface $post)
    {

        $this->tagRepo = $tag;
        $this->postRepo = $post;
    }

    public function index()
    {
        $tags = $this->tagRepo->all();
        return view('tags.index',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posts = $this->postRepo->all();
        return view('tags.create',compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagCreate $request)
    {
        $this->tagRepo->store($request);
        return redirect('tags')->with('message','Tag created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = $this->postRepo->getPosts();
        $tag = $this->tagRepo->find($id);
        return view('tags.edit',compact('tag','posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagCreate $request, $id)
    {
       if($this->tagRepo->update($id,$request)){
           return redirect('tags')->with('message','Tag updated ');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->tagRepo->delete($id)) {
            return response()->json(['id'=> $id]);
        }
        //
    }
}
