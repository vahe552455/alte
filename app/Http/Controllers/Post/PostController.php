<?php

namespace App\Http\Controllers\Post;

use App\Contracts\CategoryInterface;
use App\Contracts\PostInterface;
use App\Http\Requests\PostCreate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $postRepo;
    protected $categoryRepo;
    public function __construct(PostInterface $post,CategoryInterface $category)
    {
        $this->postRepo = $post;
        $this->categoryRepo = $category;
    }

    public function index()
    {
        $posts = $this->postRepo->getPosts();
        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories =  $this->categoryRepo->all();
        return view('posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreate $request)
    {
            $this->postRepo->store($request);
            return redirect('posts')->with('message','Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //CategoryInterface
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postRepo->find($id);
        $categories = $this->categoryRepo->all();
        return view('posts.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostCreate $request, $id)
    {
        $post = $this->postRepo->update($id, $request);
        return redirect('posts')->with('message','Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       if($this->postRepo->delete($id)) {
         return response()->json(['id'=>$id]);
       }
    }
}
