<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $fillable = ['name'];

    public function posts() {
            return $this->hasMany(Post::class);
        }
        //delete child posts
    protected static function boot() {
        parent::boot();
        static::deleting(function($offer) {
            foreach ($offer->posts as $post) {
                $post->images()->delete();
                $post->tags()->delete();
                $post->tags()->detach();
            }
            $offer->posts()->delete();
        });
    }
}
