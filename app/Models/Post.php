<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected  $fillable =['category_id','title','description'];
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    public function images() {
        return $this->hasMany(Image::class);
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }
    //delete child tags

    protected static function boot() {
        parent::boot();
        static::deleting(function($offer) {
            $offer->tags()->delete();
            $offer->tags()->detach();
            $offer->images()->delete();
        });
    }



}
