<?php
namespace App\Repositories;

use App\Contracts\PostInterface;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;

class PostRepo implements PostInterface {

    protected $model;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }
    public function getPosts( ) {
       return $this->model->with('category')->paginate(10);
    }
    public function all( ) {
       return $this->model->all();
    }
    public function store($request)
    {
        $post = $this->model->create($request->all());
        if($request->path) {
            foreach ($request->path as $image) {
                $img = Storage::disk('public')->put('images',$image);
                $post->images()->create(['path'=>$img]);
            }
        }

        // TODO: Implement store() method.

        return $post;
    }
    public function find($id)
    {
      $post = $this->model->findOrfail($id)->load('category','images');
        return $post;
    }
    public function update($id, $request)
    {
        $post = $this->model->findOrfail($id);
        $post->update($request->all());
        if($request->path) {
            foreach ($request->path as $image) {
                $img = Storage::disk('public')->put('images',$image);
                $post->images()->create(['path'=>$img]);
            }
        }
        return $post;
    }
    public function delete($id)
    {
        $post = $this->model->findOrfail($id)->delete();
        return $post;
    }
}