<?php
namespace App\Repositories;

use App\Models\Category;
use App\Contracts\CategoryInterface;

class CategoryRepo implements CategoryInterface  {

    protected $model;
    public function __construct(Category $category )
    {
        $this->model = $category;
    }

    public function all() {
        return $this->model->with('Posts')->get();
    }
    public function store($request) {
        return $this->model->create($request->all());
    }
    public function edit($id) {
        return $this->model->findOrfail($id);
    }
    public function update($id, $request) {
        $category =    $this->model->findOrfail($id);
      $category->update($request->all());
        return $category;
    }
    public function delete($id) {
        $category = $this->model->findOrfail($id);
        return $category->delete();
    }
}