<?php
namespace App\Repositories;

use App\Contracts\TagInterface;
use App\Models\Tag;

class TagRepo implements TagInterface {

    protected $tag;
    public function __construct(Tag $tag)
    {
        $this->model = $tag;
    }
    public function all() {
        return $this->model->with('posts')->get();
    }
    public function find($id) {
        return $this->model->find($id);
    }
    public function store($request) {
        $tag = $this->model->create($request->except('post_id'));
        $tag->posts()->attach($request->post_id);
        return $tag;
    }
    public function update($id,$request) {
        $tag = $this->model->findOrfail($id);
        $post = $tag->posts()->sync($request->post_id);
        return $tag;
    }
    public function delete($id) {

        return $this->model->findOrfail($id)->delete();
    }
}