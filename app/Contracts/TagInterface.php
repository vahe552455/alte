<?php

namespace App\Contracts;

Interface TagInterface {
    public function all();
    public function store($request);
    public function find($id);
    public function update($id,$request);
    public function delete($id);

}