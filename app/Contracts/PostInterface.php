<?php

namespace App\Contracts;

Interface PostInterface
{
    public function getPosts();
    public function store($request);
    public function find($id);
    public function all();
    public function delete($id);
    public function update($id,$request);

}