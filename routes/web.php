<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::Resource('/categories','Category\CategoryController');
Route::Resource('/posts','Post\PostController');
Route::Resource('/tags','Tag\TagController');
