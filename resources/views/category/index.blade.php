
@extends('layouts.app')
@section('content')

    <div id="example2_wrapper" class="dataTables_wrapper  dt-bootstrap mt-5">
        <div class="clearfix">
            <h4 class="d-inline-block">Category table</h4>
            <a href="{{route('categories.create')}}" class="btn btn-info pull-right"> Add Category</a>
        </div>
        <div class  ="row">
            <div class="col-sm-6">
                @if(session('message'))
                    <div class="alert-success mt-2 p-2">
                        {{session('message')}}
                    </div>
                @endif
            </div>
            <div class="col-sm-6"></div>
        </div>
        @if($categories->first())
            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <table id="example2" class="table table-bordered table-hover dataTable box box-info" role="grid" aria-describedby="example2_info">
                        <thead>
                        <tr role="row">
                            <th>Id</th>
                            <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Last changes</th>
                            <th> Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr role="row" class="odd" data-get="{{$category->id}}">
                                <td>{{$category->id}}</td>
                                <td class="sorting_1"><a href="{{route('categories.edit',$category->id)}}">{{$category->name}}</a>
                                <td>{{$category->updated_at}}</td>
                                <td ><a href="{{route('categories.destroy',$category->id)}}" class="btn btn-danger destroy" data-method="delete"  data-id="{{$category->id}}">Delete</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                </div>
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                            <li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li>
                            <li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li>
                            <li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
    </div>
    @else
    <div class="alert-info p-3">
        You don't have now any categories please add
    </div>
    @endif
@endsection