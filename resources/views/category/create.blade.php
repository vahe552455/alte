
@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="box box-info col-8">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('categories.store')}}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Name" name="name">
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{route('categories.index')}}"  class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-info pull-right">Add</button>
                            @if(session('message'))
                            <div class="alert-success mt-2 p-2">
                                {{session('message')}}
                            </div>
                            @endif
                            @if($errors->any())
                                <div class="alert-danger mt-2 p-2">
                                    {{$errors->first()}}
                                </div>
                                @endif
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>

            </div>
        </div>
    </div>
@endsection