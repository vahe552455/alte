
@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="box box-info col-8">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Name" name="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea name="description"  class="form-control" placeholder="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Select</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category )
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-sm-10 float-right">
                                <input type="file" id="exampleInputFile" name="path[]" multiple="multiple">
                                <p class="help-block">Upload file</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{route('posts.index')}}"  class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-info pull-right">Add</button>
                            @if(session('message'))
                                <div class="alert-success mt-2 p-2">
                                    {{session('message')}}
                                </div>
                            @endif
                            @if($errors->any())
                                <div class="alert-danger mt-2 p-2">
                                    {{$errors->first()}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>

            </div>
        </div>
    </div>
@endsection