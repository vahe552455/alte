
@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="box box-info col-8">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form class="form-horizontal" action="{{route('posts.update',$post->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Name" name="title" value="{{$post->title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea name="description"  class="form-control" placeholder="description">{{$post->description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Select</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category )
                                        <option value="{{$category->id}}" {{$post->category_id == $category->id ?' selected':''}}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-sm-10 float-right">
                                <input type="file" id="exampleInputFile" name="path[]" multiple="multiple">
                                <p class="help-block">Upload file</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{route('posts.index')}}"  class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-info pull-right">Update</button>
                            @if(session('message'))
                                <div class="alert-success mt-2 p-2">
                                    {{session('message')}}
                                </div>
                            @endif
                            @if($errors->any())
                                <div class="alert-danger mt-2 p-2">
                                    {{$errors->first()}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>
                <div class="container mt-5">

                    <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Thumbnail Gallery</h1>

                    <hr class="mt-2 mb-5">

                    <div class="row text-center text-lg-left">
                        @foreach($post->images as $image)
                        <div class="col-lg-3 col-md-4 col-6">
                            <a href="#" class="d-block mb-4 h-100">
                                <img class="img-fluid img-thumbnail" src="{{asset('storage/'.$image->path)}}" alt="">
                            </a>
                        </div>

                        @endforeach

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection