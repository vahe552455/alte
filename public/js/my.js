$(document).ready(function () {
    $(document).on('click','.destroy', (e)=> {
        event.preventDefault();
        var id = $(e.target).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax( {
            method: 'DElETE',
            url: $(e.target).attr('href'),
            data:{
                id:id
            },
            success:function (data) {
console.log(data.id)
                $("[data-get='"+data.id+"']").remove();
            },
            error: function (error) {
                console.log(error)
            }
        })
    })
})
